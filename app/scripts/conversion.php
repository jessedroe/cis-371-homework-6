<?php 

// Pull in JSON from url
$json = file_get_contents("http://www.cis.gvsu.edu/~scrippsj/cs371/hw/fortune500.json");

// Create an array from JSON file (actually an array of arrays)
$obj = json_decode($json, true);

// Save off the main array value (result) to a var for easier access later
$objArr = $obj['result'];

// Create a DOMDoc object
$doc = new DOMDocument('1.0');
$doc->encoding = "ISO-8859-1";

// Create the root of the XML file
$root = $doc->createElement('companies');
$root = $doc->appendChild($root);

// Populates the XML file with the data from the JSON array
for($i = 0; $i < count($objArr); $i++) {
	// echo $objArr[$i]['Company'] . "<br>";
	$comp = $doc->createElement('comp');
	$comp = $root->appendChild($comp);

	// Create nodes and fill with data from JSON array
	$comp->appendChild($doc->createElement('year', $objArr[$i]['Year']));
	$comp->appendChild($doc->createElement('rank', $objArr[$i]['Rank']));
	$comp->appendChild($doc->createElement('revenue', $objArr[$i]['Revenue']));
	$comp->appendChild($doc->createElement('profit', $objArr[$i]['Profit']));
	$comp->appendChild($doc->createElement('company', $objArr[$i]['Company']));
}

// Enable formatting in the DOM object
$doc->formatOutput = true;
$output = $doc->saveXML();

// Saves DOMDocument to file in scripts directory.
$doc->save('companies.xml');

// Echos the xml to the view
echo $output;

?>